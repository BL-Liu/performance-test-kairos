Add new packages or update their versions in `requirements.in`, then run:

```
bazel run //performance_report/third_party/python:requirements.update
```
